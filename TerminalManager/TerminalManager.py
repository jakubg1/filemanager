import curses, os
from ..StringFormatter.StringFormatter import StringFormatter
from ..DirsInterface.DirsInterface import DirsInterface

ESC_KEY = 27
ENTER_KEY = 10

class TerminalManager:

    def __init__(self, stdscr):
        self.setColorPairs()
        self.maxHeight, self.maxWidth = stdscr.getmaxyx()
        self.stdscr = stdscr
        self.dirsInterface = DirsInterface()

        self.filePad = curses.newpad(10000, self.maxWidth)
        self.filePad.keypad(True)
        self.currentPointer = 0
        self.selected = []
        self.toCopy = []

        self.stdscr.refresh()

        self.savedCurrents = {}

        self.currentLen = 0
        self.padBeginHeight = 0

        self.printSchema()
        self.printFiles()
        self.fileLen = None

        self.presentingFile = False
        self.handlingInput()

    def saveCurrent(self):
        self.savedCurrents[self.dirsInterface.getPath()] = self.currentPointer

    def printSchema(self):
        self.stdscr.clear()
        self.stdscr.border()
        pathStr = StringFormatter.middle(f"PATH: {self.dirsInterface.getPath()}", self.maxWidth - 2)
        self.stdscr.addstr(1, 1, pathStr, self.WHITE_GREEN)
        headerStr = StringFormatter.middle("FILE/DIR : NAME : SIZE(?)", self.maxWidth - 2)
        self.stdscr.addstr(2, 1, headerStr, self.MAGENTA_BLACK)
        self.stdscr.refresh()

    def printFile(self, i, item, STYLE = None):

        if self.dirsInterface.isDir(item):
            type = "DIR"
        else:
            type = "FILE"

        sizeData = self.dirsInterface.getSizeOfFile(item)

        if STYLE == None:
            if self.currentPointer == i: STYLE = self.BLACK_BLUE
            elif i in self.selected: STYLE = self.BLACK_MAGENTA
            else: STYLE = self.BLUE_BLACK

        line = StringFormatter.cols([type,  item, str(round(float(sizeData["size"]),2)) + sizeData["unit"]], self.maxWidth - 2)
        self.filePad.addstr(i, 0, line, STYLE)

    def printFiles(self, currentFolder = None):
        self.padBeginHeight = 0
        self.files = self.dirsInterface.getFiles()
        self.currentLen = len(self.files)
        self.filePad.clear()

        for i, item in enumerate(self.files): 
            if currentFolder != None and currentFolder == item: self.currentPointer = i
            
        while self.currentPointer > (self.padBeginHeight + self.maxHeight - 5): self.padBeginHeight += 1


        for i, item in enumerate(self.files): 
            self.printFile(i, item)

        self.filePad.refresh(self.padBeginHeight, 0, 3, 1, self.maxHeight - 2, self.maxWidth - 2)

    def updateCurrent(self, down = None, oldCurrent = None):
        if down == 1:
            self.printFile(self.currentPointer-1, self.files[self.currentPointer-1])
        elif down == 0: self.printFile(self.currentPointer+1, self.files[self.currentPointer+1])
        else:
            if oldCurrent != None: self.printFile(oldCurrent, self.files[oldCurrent])
        self.printFile(self.currentPointer, self.files[self.currentPointer])

    def changeFolder(self, filename = None):
        self.saveCurrent()
        if filename == None:
            currentFolder = self.dirsInterface.goParentFolder()
        else:
            self.dirsInterface.goForward(filename)
            currentFolder = None
        
        if self.dirsInterface.getPath() in self.savedCurrents.keys(): self.currentPointer = self.savedCurrents[self.dirsInterface.getPath()]
        else: self.currentPointer = 0

        self.printFiles(currentFolder)
        pathStr = StringFormatter.middle(f"PATH: {self.dirsInterface.getPath()}", self.maxWidth - 2)
        self.stdscr.addstr(1, 1, pathStr, self.WHITE_GREEN)

    def printFileLines(self):
        self.filePad.clear()
        lines = self.dirsInterface.getContentOfFile(self.files[self.currentPointer])
        for i, line in enumerate(lines): self.filePad.addstr(i, 0,str(i+1) + " " + line, self.WHITE_BLACK)

        return len(lines)
    
    def confirmation(self):
        key = self.stdscr.getch()

        if key in [ord('Y'), ord('y'), ENTER_KEY]: return True
        
        return False

    def removeCurrent(self):
        self.confirmPad = curses.newpad(1,30)
        self.confirmPad.keypad(True)
        self.stdscr.refresh()

        self.confirmPad.addstr(0, 0, "Do you want to del this file?", self.BLACK_MAGENTA)
        self.confirmPad.refresh(0, 0, 0, 2, 1, 30)

        if self.confirmation(): 
            if len(self.selected) > 0: 
                for fileIndex in self.selected: self.dirsInterface.removeFile(self.files[fileIndex])
                self.selected = []
            else: self.dirsInterface.removeFile(self.files[self.currentPointer])

        self.printSchema()
        if len(self.dirsInterface.getFiles()) <= 0: self.currentPointer = None
        else:
            while len(self.dirsInterface.getFiles()) < self.currentPointer + 1: self.currentPointer -= 1
        self.printFiles()

    def searchInDir(self):

        query = ""

        self.stdscr.addstr(0, 2, f"/ {query}")

        iterator = 0
        buffer = []

        while True:
            length = len(query)

            key = self.stdscr.getch()
            if key in [ESC_KEY, curses.KEY_DOWN, curses.KEY_UP, curses.KEY_LEFT, curses.KEY_RIGHT]:
                self.printSchema()
                break
            elif key == ENTER_KEY:
                if query != "": 
                    if iterator < len(buffer) - 1: iterator += 1
                    else: iterator = 0
                    if len(buffer) > 0: self.padBeginHeight = buffer[iterator]
            else:
                buffer = []
                iterator = 0
                if key == curses.KEY_BACKSPACE:
                    query = query[:-1]
                else:
                    query += chr(key)
                for i, line in enumerate(self.files):
                    if query in line: buffer.append(i)

                oldCurrent = self.currentPointer
                
                if len(buffer) > 0: 
                    self.padBeginHeight = buffer[iterator]
                    self.currentPointer = buffer[iterator]
                else: 
                    self.padBeginHeight = 0
                    self.currentPointer = 0

            res = ""
            for i in range(length): res += " "
            self.stdscr.addstr(0, 2, f"/ {res}")
            self.stdscr.addstr(0, 2, f"/ {query}")
            
            self.updateCurrent(None, oldCurrent)
            
            self.filePad.refresh(self.padBeginHeight, 0, 3, 1, self.maxHeight - 2, self.maxWidth - 2)
    
    def searchInFile(self):

        query = ""

        self.stdscr.addstr(0, 2, f"/ {query}")
        self.filePad.refresh(self.padBeginHeight, 0, 3, 1, self.maxHeight - 2, self.maxWidth - 2)

        iterator = 0
        buffer = []

        while True:
            length = len(query)

            key = self.stdscr.getch()
            if key in [ESC_KEY, curses.KEY_DOWN, curses.KEY_UP, curses.KEY_RIGHT, curses.KEY_LEFT]:
                self.printSchema()
                break
            elif key == ENTER_KEY:
                if query != "": 
                    if iterator < len(buffer) - 1: iterator += 1
                    else: iterator = 0
                    if len(buffer) > 0: self.padBeginHeight = buffer[iterator]
            else:
                buffer = []
                iterator = 0
                if key == curses.KEY_BACKSPACE: query = query[:-1]
                else: query += chr(key)
                lines = self.dirsInterface.getContentOfFile(self.files[self.currentPointer])
                for i, line in enumerate(lines):
                    if query in line: buffer.append(i)
                if len(buffer) > 0: self.padBeginHeight = buffer[iterator]
                else: self.padBeginHeight = 0

            res = ""
            for i in range(length): res += " "
            self.stdscr.addstr(0, 2, f"/ {res}")
            self.stdscr.addstr(0, 2, f"/ {query}")

            self.filePad.refresh(self.padBeginHeight, 0, 3, 1, self.maxHeight - 2, self.maxWidth - 2)

    def createFile(self, isFile = True):
        self.stdscr.addstr(0, 2, f"Name: ")

        filename = ""

        while True:
            key = self.stdscr.getch()

            filenameLen = len(filename)
            responseClearer = ""
            for i in range(filenameLen): responseClearer += ' '

            if key in [ENTER_KEY]: break
            elif key in [ESC_KEY, curses.KEY_LEFT, curses.KEY_RIGHT, curses.KEY_UP, curses.KEY_DOWN]: 
                self.printSchema()
                return 
            elif key in [curses.KEY_BACKSPACE]: filename = filename[:-1]
            else: filename = filename + chr(key)

            self.stdscr.addstr(0, 2, f"Name: {responseClearer}")
            self.stdscr.addstr(0, 2, f"Name: {filename}")


        if ' ' in filename: filename = filename.split()

        if isFile:
            if filename == "": filename = "example.txt"
            result = self.dirsInterface.createFile(filename)
        else: 
            if filename == "": filename = "example_dir"
            result = self.dirsInterface.createDir(filename)
        
        if not result: self.stdscr.addstr(0, 2, f"Action failed!")
        
        self.printSchema()
        self.printFiles()

    def markDirs(self):

        self.stdscr.addstr(0, 2, "VISUAL")

        self.selected = []

        while True:
            key = self.stdscr.getch()

            if key in [ord('v'), ord('V')]: 
                if self.currentPointer in self.selected: 
                    for i, item in enumerate(self.selected): 
                        if item == self.currentPointer: self.selected.pop(i)
                else: 
                    self.selected.append(self.currentPointer)
                    self.printFile(self.currentPointer, self.files[self.currentPointer])
            elif key in [curses.KEY_UP, ord('w'), ord('W')]:
                if self.currentPointer > 0:
                    if self.currentPointer == self.padBeginHeight:
                        self.padBeginHeight -= 1
                    self.currentPointer -= 1
                    self.updateCurrent(0)
            elif key in [curses.KEY_DOWN, ord('s'), ord('S')]:
                if self.currentPointer < self.currentLen - 1:
                    if self.currentPointer == self.maxHeight - 5 + self.padBeginHeight: 
                        self.padBeginHeight += 1
                    self.currentPointer += 1
                    self.updateCurrent(1)
            elif key in [ord('y'), ord('Y')]: 
                path = self.dirsInterface.getPath()
                if not path.endswith('/'): path += "/"
                for file in self.selected: self.toCopy.append(path.replace(" ","") + self.files[file].replace(" ",""))
                break
            elif key in [curses.KEY_DC]:
                self.removeCurrent()
                break
            elif key in [ESC_KEY]: 
                break

            self.filePad.refresh(self.padBeginHeight, 0, 3, 1, self.maxHeight - 2, self.maxWidth - 2)


        self.selected = []
        self.printSchema()
        self.printFiles()

    def paste(self):
        copyingFiles = self.toCopy

        if len(copyingFiles) > 0:  
            self.dirsInterface.copyFile(copyingFiles)
            self.toCopy = []
            self.printFiles()

    def copyCurrent(self):
        self.toCopy = [self.dirsInterface.getCurrentDirectory() + self.files[self.currentPointer]]
        
    def handlingInput(self):
        while True:
            key = self.stdscr.getch()

            if key in [ord('Q'), ord('q'), ESC_KEY]: break

            if not self.presentingFile:
                if key in [curses.KEY_UP, ord('w'), ord('W')]:
                    if self.currentPointer > 0:
                        if self.currentPointer == self.padBeginHeight:
                            self.padBeginHeight -= 1
                        self.currentPointer -= 1
                        self.updateCurrent(0)
                elif key in [curses.KEY_DOWN, ord('s'), ord('S')]:
                    if self.currentPointer < self.currentLen - 1:
                        if self.currentPointer == self.maxHeight - 5 + self.padBeginHeight:
                            self.padBeginHeight += 1
                        self.currentPointer += 1
                        self.updateCurrent(1)
                elif key in [curses.KEY_RIGHT, curses.KEY_ENTER, ENTER_KEY, ord('d'), ord('D')]:
                    if len(self.files) > 0:
                        if self.dirsInterface.isDir(self.files[self.currentPointer]): self.changeFolder(self.files[self.currentPointer])
                        else:
                            self.presentingFile = True
                            self.fileLen = self.printFileLines()
                elif key in [curses.KEY_LEFT, curses.KEY_BACKSPACE, ord('a'), ord('A')]:
                    self.changeFolder(None)
                elif key == curses.KEY_DC:
                    self.removeCurrent()
                elif key in [ord('/')]:self.searchInDir()
                elif key in [ord('t'), ord('T')]: self.createFile(True)
                elif key in [ord('m'), ord('M')]: self.createFile(False)
                elif key in [ord('p'), ord('P')]: self.paste()
                elif key in [ord('v'), ord('V')]: self.markDirs()
                elif key in [ord('y'), ord('Y')]: self.copyCurrent()


            else:
                if key in [curses.KEY_UP, ord('w'), ord('W')]:
                    if not self.padBeginHeight == 0: self.padBeginHeight -= 1
                elif key in [curses.KEY_DOWN, curses.KEY_ENTER, ENTER_KEY, ord('s'), ord('S')]:
                    if self.padBeginHeight < self.fileLen: self.padBeginHeight += 1
                elif key in [curses.KEY_LEFT, curses.KEY_BACKSPACE, ord('a'), ord('A')]:
                    self.presentingFile = False
                    self.fileLen = None
                    self.printFiles()
                elif key in [ord('/')]:
                    self.searchInFile()

            self.filePad.refresh(self.padBeginHeight, 0, 3, 1, self.maxHeight - 2, self.maxWidth - 2)

    def setColorPairs(self):
        curses.start_color()
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_CYAN)
        curses.init_pair(2, curses.COLOR_BLACK, curses.COLOR_BLUE)
        curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_GREEN)
        curses.init_pair(4, curses.COLOR_WHITE, curses.COLOR_YELLOW)
        curses.init_pair(5, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
        curses.init_pair(6, curses.COLOR_BLUE, curses.COLOR_BLACK)
        curses.init_pair(7, curses.COLOR_BLACK, curses.COLOR_MAGENTA)
        curses.init_pair(8, curses.COLOR_WHITE, curses.COLOR_BLACK)


        self.BLACK_CYAN = curses.color_pair(1)
        self.BLACK_BLUE = curses.color_pair(2)
        self.WHITE_GREEN = curses.color_pair(3)
        self.WHITE_YELLOW = curses.color_pair(4)
        self.MAGENTA_BLACK = curses.color_pair(5)
        self.BLUE_BLACK = curses.color_pair(6)
        self.BLACK_MAGENTA = curses.color_pair(7)
        self.WHITE_BLACK = curses.color_pair(8)
