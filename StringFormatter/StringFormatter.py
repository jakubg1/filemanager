class StringFormatter:
    # static method that returns string with text and pad on the right side
    @staticmethod
    def setwR(text:str , width:int) -> str:
        while len(text) < width: text = text + " "

        if width < len(text): text = text[:width-3] + ".."

        return text
    
    # static method that returns string with text and pad on the left side
    @staticmethod
    def setwL(text:str , width:int) -> str:
        while len(text) < width: text = " " + text

        if width < len(text): text = text[:width-3] + ".."

        return text

    # static method that returns string with texts and equal spaces between them
    @staticmethod
    def cols(texts: list, width:int) -> str:

        totalWidth = 0

        for text in texts: totalWidth += len(text)

        if totalWidth > width:
            
            texts[1] = texts[1][:9] + "... " + texts[1][texts[1].index("."):]
            
        totalWidth = 0
        
        for text in texts: totalWidth += len(text)

        space = width - totalWidth
        eachSpace = int(space / (len(texts)-1))
        space = space - eachSpace*(len(texts)-1)

        for i in range(len(texts)-1):
            for j in range(eachSpace):
                texts[i] += " "

        i = 0

        while space > 0 :
            texts[i] += " "
            space -= 1
            i += 1

        return "".join(texts)

    # static method that returns STRING with text and pad on the both sides
    @staticmethod
    def middle(text:str, width:int) -> str:
        while len(text) < width: 
            text = text + " "
            if len(text) < width: text = " " + text

        if width < len(text): text = text[:width-3] + ".."

        return text
