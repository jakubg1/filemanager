# Filemanager

The **filemanager** program was written in Python using the **Curses** library and is designed to manage folders and files on a Linux system. It allows users to navigate between directories and files, create, delete, copy, select (multiple at once), and search. Thanks to the Curses library, the terminal window does not refresh every time the user performs an action.

To run the program you have to be in the parent folder of the project and type in terminal `python3 -m parentFolder.main` and press the **Enter** key.

## Navigating Between Files

To navigate between files, you can use the arrow keys or the following letters:
- **w** - Move up
- **s** - Move down
- **a** - Move to the parent directory
- **d** - Move to the selected (highlighted) directory or file

## Selecting Multiple Files at Once

The program allows you to select multiple files at once for bulk deletion or copying. To enter selection mode, press the letter **v** (from "visual"). To select a file, press **v** again. To exit this mode, press the letter **q**.

## Creating Files and Folders

To create a text file, press the letter **t** (from "touch") and then enter the name of the file (or multiple files separated by spaces). Creating folders works similarly, but you press the letter **m** (from "mkdir" - make directory).

## Deleting Files and Folders

Deleting folders is done by pressing the **DELETE** key and confirming the deletion with the **ENTER** key or the letter **y**.

## Copying and Pasting Files

Copying files is done by pressing the **y** key. Pasting is possible using the **p** key.

## Searching Inside a File or Folder

Searching inside a file or folder is possible by pressing the **/** key. Then, enter the search phrase. The line with the next occurrence will appear after pressing the **ENTER** key.
