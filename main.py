from curses import wrapper
from .TerminalManager.TerminalManager import TerminalManager

def main(stdscr):
    termMan = TerminalManager(stdscr)


if __name__ == '__main__':
    wrapper(main)