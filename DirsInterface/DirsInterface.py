import os

class DirsInterface:

    def __init__(self):
        self.path = os.getcwd()

    def getPath(self):
        return self.path
    
    def getCurrentDirectory(self):
        path = self.getPath()
        if not path.endswith('/'): path += "/"

        return path
    
    def removeFile(self, filename):
        os.system(f"rm -R -f {self.path + "/" + filename}")

    def goParentFolder(self):
        current = self.path
        if len(self.path[:self.path.rindex("/")]) > 0: self.path = self.path[:self.path.rindex("/")]
        else: self.path = self.path[:self.path.rindex("/")+1]
        return current[current.rindex("/")+1:]
        
    def goForward(self, dir):
        try: 
            if self.path != "/": self.path = self.path + "/" + dir
            else: self.path = self.path + dir
            return self.getFiles()
        except:
            return []
    
    def getFiles(self):
        try:
            self.files = sorted(os.listdir(self.path))
            return sorted(self.files)
        except Exception:
            return []
    
    def isDir(self, name):
        return os.path.isdir(self.path + "/" + name)
    
    def getSizeOfFile(self, name):
        try:
            size = os.stat(self.path + "/" + name).st_size

            unitsList = ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"]
            unit = 0

            while size > 999:
                size /= 1024
                unit += 1
            
            return {"size": round(size,2), "unit":unitsList[unit]}
        except Exception:
            return {"size": 0, "unit": "-"}

    def getContentOfFile(self, filename):
        try:
            with open(self.path + "/" + filename) as f:
                return f.readlines()
        except Exception as e:
            return []

    def createFile(self, filename):
        try:
            if isinstance(filename, list): 
                for file in filename:
                    if self.path != "/": path = self.path + "/"
                    else: path = "/"
                    os.system(f"touch {path + file}")
            else:
                if self.path != "/": path = self.path + "/"
                else: path = "/"
                os.system(f"touch {path + filename}")
            return True
        except:
            return False
        
    def createDir(self, dirname):
        try:
            if isinstance(dirname, list): 
                for dir in dirname:
                    if self.path != "/": path = self.path + "/"
                    else: path = "/"
                    os.system(f"mkdir {path + dir}")
            else:
                if self.path != "/": path = self.path + "/"
                else: path = "/"
                os.system(f"mkdir {path + dirname}")
            return True
        except:
            return False
        
    def copyFile(self, filesToCopy):

        path = self.getCurrentDirectory()

        for file in filesToCopy: 

            i = 1
            newFileName = file

            while os.path.isdir(newFileName) or os.path.isfile(newFileName): 
                newFileName += "_" + str(i)
                i += 1
            command = "cp" + f" -r {file} " + newFileName

            os.system(command)
            